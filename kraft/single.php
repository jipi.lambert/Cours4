<?php

get_header();
?>
<?php
while (have_posts()) :
	the_post();
?>
	<h1><?php the_title(); ?></h1>
<?php
	the_post_navigation(
		array(
			'prev_text' => '<span class="screen-reader-text">' . __('Previous Post', 'kraft') . '</span><span aria-hidden="true" class="nav-subtitle">' . __('Previous', 'twentyseventeen') . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper"><--</span>%title</span>',
			'next_text' => '<span class="screen-reader-text">' . __('Next Post', 'kraft') . '</span><span aria-hidden="true" class="nav-subtitle">' . __('Next', 'twentyseventeen') . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">--></span></span>',
		)
	);
endwhile; // End the loop.
?>
<?php
get_footer();
