<?php
namespace Kraft;

class Enqueues{
    public function __construct(){
        add_action('wp_enqueue_scripts', [$this,'scripts']);
    }

    public function scripts(){
        wp_enqueue_style("kraft_bootstrap", get_theme_file_uri("/styles/style.css"), array(), "0.3");
        wp_enqueue_script("script-js", get_theme_file_uri("/js/script.js"), array(), "0.1", true);
        wp_deregister_script( 'jquery' );
        wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.5.1.min.js", array(), '3.5.1', false );
    }
}