<?php

namespace Kraft;

class FormHelper
{
    public function __construct()
    {
        add_action('template_redirect', [$this, 'traitement_formulaire_contact']);
    }

    public function traitement_formulaire_contact()
    {
        // Adresse d'ou le formulaire a été envoyé tout en retirant les arguments de requete
        $url_ref = remove_query_arg(['added','duplicate'], wp_get_referer() );
        if (!isset($_POST['envoyer']) || !isset($_POST['register-entreprise'])) {
            return;
        }
        // Petite mesure de sécurité si ne nonce n,est pas valide on retourne et on ne fait rien.
        if (!wp_verify_nonce($_POST['register-entreprise'], 'entreprise')) {
            // Je retourne une valeur pour signifier que le nonce es invalide
            $url = add_query_arg('nonce invalide', 1, $url_ref);
            wp_safe_redirect($url);
            exit();
        }

        // avant de proceer je revalide le courriel
        if (!filter_var($_POST["courriel"], FILTER_VALIDATE_EMAIL)) {
            $url = add_query_arg('erreur', 'courriel invalide', $url_ref);
            wp_safe_redirect($url);
            exit();
        } else {
            // Verifier si le contact existe si il existe envoyer une erreur de duplication
            if (is_null(get_page_by_title($_POST["courriel"], 'OBJECT', 'kraft_contact'))) {
                // je prepare la creation de mon post
                
                $args = array(
                    'comment_status'    =>    'closed',
                    'ping_status'        =>    'closed',
                    'post_author'        =>    1,
                    'post_name'            =>    $_POST["courriel"],
                    'post_title'         =>    $_POST["courriel"],
                    'post_status'        =>    'publish',
                    'post_type'            =>    'kraft_contact'
                );
                // J'insere le post et recupere le id
                $post_id = wp_insert_post($args);

                // j'ajoute les meta du post
                // Je renvoi la mention d'ajout a la page du formulaire
                $url = add_query_arg('added', 1, $url_ref);
            } else {
                // Je renvoi la mention de duplicata a la page du formulaire
                $url = add_query_arg('duplicate', 1, $url_ref);
            }

            //je retourne les informations a la page
            wp_safe_redirect($url);
        }
    }

    public function add_meta($post_id, $post_data)
    {
        $keys = ["prenom", "nom", "courriel", "entreprise", "description"];

        // Pour chacune des informations dans ma variable POST
        foreach ($post_data as $key => $data) {
            // Si ce sont des champs que je veux traiter
            if (in_array($key, $keys)) {
                //Si ce n'et pas la cle courriel nettoie les donnees
                if ($key != "courriel") {
                    // Retirons les caratères spéciaux.
                    $data = sanitize_text_field($data);
                    $meta_id = add_post_meta($post_id, $key, $data);
                } else {
                    // ajoute les meta en lien avec le post
                    $data = sanitize_email($data);
                    $meta_id = add_post_meta($post_id, $key, $data);
                }
            }
        }
        // Retourne la valeur erreur
        return $meta_id;
    }
}
