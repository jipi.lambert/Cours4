<?php 
namespace Kraft;
use Kraft\Contact;
use Kraft\Enqueues;
use Kraft\FormHelper;
use Kraft\ShortCode\ContactShortcode;

class Init{
    public function __construct(){
        add_action("init", [$this,"kraft_setup"]);
        add_action("init", [$this, "create_posttypes"]);
        new FormHelper();
        new Enqueues();
        new ContactShortcode();
    }

    public function create_posttypes(){
        new Contact();

    }

    public function enqueue_scripts(){
       
    }

    public function kraft_setup(){
        add_theme_support(
            'html5',[
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'script',
                'style',
                'navigation-widgets',
            ]
        );
    
        add_theme_support(
            'post-formats',array(
                'aside',
                'image',
                'video',
                'quote',
                'link',
                'gallery',
                'audio',  
            )
        );
    }
}
