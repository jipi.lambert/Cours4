<?php
get_header();
?>
  <main class="container">

<?php 
    get_template_part("template-parts/content/content","hero");
    get_template_part("template-parts/content/content","news");
    get_template_part("template-parts/content/content","articles");
    get_sidebar();
?>
    
  </main><!-- /.container -->

<?php 
get_footer();
?>