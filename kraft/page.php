<?php

get_header('pages');
?>
<?php
while (have_posts()) :
	the_post();
?>
	<h1><?php the_title(); ?></h1>
	<p><?php the_content();?></p>
<?php
endwhile; // End the loop.
?>
<?php
get_footer();
