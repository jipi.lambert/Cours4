<?php
use Kraft\Contact;
$the_query = Contact::get_all_contact();

?>
<div class="row mb-2">
<?php 
    if($the_query->have_posts()){
        while($the_query->have_posts()){
            $the_query->the_post();
    ?>
     
    <div class="col-md-6">
        <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-primary"><?php the_title(); ?></strong>
                <h3 class="mb-0">Featured post</h3>
                <?php 
                if (get_field('age')){
                ?>
                    <p><?php the_field("age"); ?></p>
                <?php
                    } 
                ?>
                <p><?php the_field("courriel"); ?></p>
                <div class="mb-1 text-muted"><?php the_date(); ?></div>
                <p class="card-text mb-auto"><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" class="stretched-link"><?php the_permalink(); ?></a>
            </div>
            <div class="col-auto d-none d-lg-block">
                <svg class="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false">
                    <title>Placeholder</title>
                    <rect width="100%" height="100%" fill="#55595c" /><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                </svg>

            </div>
        </div>
    </div>
    <?php 
    }
}
?>
</div>