<div class="row">
  <div class="col-md-8">
    <h3 class="pb-4 mb-4 font-italic border-bottom">
      From the Firehose
    </h3>

    <article class="blog-post">
      <?php
      if (have_posts()) {
        while (have_posts()) {
          the_post();?>
          <h2 class="blog-post-title"><?php the_title();?></h2>
          <p class="blog-post-meta"><?php the_date();?> by <a href="#"><?php the_author();?></a></p>

          <?php the_content(); ?>
          <a href="<?php the_permalink(); ?>">Lien</a>
    </article><!-- /.blog-post -->
<?php
        }
      }

      the_posts_pagination(
        [
          'prev_text' => "<--",
          'next_text' => "-->",
          'before_page_number' => __('Page', 'kraft')
        ]
      )
?>
<nav class="blog-pagination" aria-label="Pagination">
  <a class="btn btn-outline-primary" href="#">Older</a>
  <a class="btn btn-outline-secondary disabled" href="#" tabindex="-1" aria-disabled="true">Newer</a>
</nav>

  </div>