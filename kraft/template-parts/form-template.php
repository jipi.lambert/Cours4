<?php

/**
 * Template Name: Custom form template
 */
get_header();

?>
<main class="container">
    <section>
        <header>
            <h1><?php the_title();?></h1>
        </header>
        <?php
        if (isset($_GET['duplicate'])){
            ?>
                <div class="error">Ce client existe déjà dans notre base de donnée.</div>
            <?php
        }

        if (isset($_GET['added'])){
            ?>
                <div class="success">Vous avez ete ajoute a la base de donnee de client.</div>
            <?php
        }
        ?>
        <div class="form">
            <!-- <form action="#" class="<?php if (isset($_GET['duplicate'])){?> field_error <?php } ?>" method="POST">
                <?php wp_nonce_field( 'entreprise', 'register-entreprise' ); ?>
                <input id="prenom" type="text" name="prenom" placeholder="Votre prénom">
                <input id="nom" type="text" name="nom" placeholder="Votre nom">
                <input type="email" name="courriel" id="courriel" placeholder="Votre courriel">
                <input type="text" name="entreprise" id="entreprise" placeholder="Nom de votre entreprise">
                <textarea name="description" id="description" placeholder="Courte description de votre entreprise."></textarea>
                <input id="submit" type="submit" name="envoyer" id="submit" value="<?php esc_attr_e( 'Inscrire votre entreprise', 'kraft' ); ?>" />
            </form> -->
            <?php
            echo do_shortcode(' [kraft_contact_registration] ');
            ?>
        </div>
    </section>
</main><!-- /.container -->
<?php
get_footer();
