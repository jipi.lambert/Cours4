<?php 
require __DIR__ . '/vendor/autoload.php';
use Kraft\Init;

add_action("init", "kraft_menus");


function kraft_menus(){
    register_nav_menus(
        array(
            'main'  => __("Main menu", "kraft")
        )
    );
}
add_theme_support( 'title-tag' );

new Init();